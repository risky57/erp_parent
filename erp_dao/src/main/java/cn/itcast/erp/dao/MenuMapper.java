package cn.itcast.erp.dao;

import cn.itcast.erp.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 创建人: 武奇
 * 创建事件: 2019/6/3
 */
public interface MenuMapper {

    List<Menu> findChildrenByPid(@Param("empId") Long empId, @Param("pid") String pid);

    List<Menu> findAllByEmpId(@Param("empId") Long empId);


}
