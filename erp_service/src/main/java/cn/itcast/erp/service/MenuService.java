package cn.itcast.erp.service;

import cn.itcast.erp.entity.Menu;

import java.util.List;

/**
 * 创建人: 武奇
 * 创建事件: 2019/6/3
 */
public interface MenuService {
    List<Menu> findByEmpId(Long uuid);
}
