package cn.itcast.erp.service;

import cn.itcast.erp.entity.Menu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 创建人: 武奇
 * 创建事件: 2019/6/4
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:application-*.xml")
public class MenuServiceTest {

    @Resource
    private MenuService menuService;

    @Test
    public void findByEmpId() {
        List<Menu> menus = this.menuService.findByEmpId(1L);
        this.printTree(menus);
    }

    private void printTree(List<Menu> menus) {
        this.printTree(menus, 0);
    }

    private void printTree(List<Menu> menus, int index) {
        for (Menu menu : menus) {
            for (int i = 0; i < index; i++) {
                System.out.print("\t");
            }
            System.out.println(menu);
            List<Menu> children = menu.getMenus();
            if (children != null && children.size() > 0) {
                printTree(children, index + 1);
            }
        }
    }
}